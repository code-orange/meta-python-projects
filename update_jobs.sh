#!/bin/bash

urls=`git submodule foreach git config --get remote.origin.url | grep -E '(http|https)://'`

echo "node: {" > Jenkinsfile
echo "    stage 'build'" >> Jenkinsfile

while read -r line; do
    echo "    build job: 'django_apps_req_build_trunk', propagate: false, parameters: [[\$class: 'StringParameterValue', name: 'GIT_URL', value: '$line']]" >> Jenkinsfile
    echo "    build job: 'django_apps_req_build_docs', propagate: false, parameters: [[\$class: 'StringParameterValue', name: 'GIT_URL', value: '$line']]" >> Jenkinsfile
done <<< "$urls"

echo "}" >> Jenkinsfile

git add Jenkinsfile

git commit -a -m "update Jenkinsfile"
git push origin master
